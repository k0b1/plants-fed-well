import { mount } from '@vue/test-utils'
//import Vue from 'vue'
// import Vuetify from 'vuetify'
import ImageAndContent from '@/components/image-and-content.vue'

describe('image-and-content.vue', () => {
  // beforeEach(() => {
  //   Vue.use(Vuetify)
  // })
  it('is Vue instance', () => {
    const wrapper = mount(ImageAndContent, {
        propsData: {
            image: 'feed-plant-water.jpg',
            size: 'desktop',
            text: 'default text',
            title: 'default title'
        }
    })
    expect(wrapper.vm).toBeTruthy()
  })
})
