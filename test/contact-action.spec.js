import { mount, RouterLinkStub } from '@vue/test-utils'
import ContactAction from '@/components/contact-action.vue'

describe('contact-action.vue', () => {
  it('is Vue instance', () => {
    const wrapper = mount(ContactAction, {
      stubs: {
        NuxtLink: RouterLinkStub,
      },
    })
    expect(wrapper.vm).toBeTruthy()
  })
})
