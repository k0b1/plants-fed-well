import { shallowMount } from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import CultivationConsulting from './../../pages/cultivation-consulting/index.vue'

describe('pages/cultivation-consulting/index.vue', () => {
  beforeEach(() => {
    Vue.use(Vuetify)
  })
  test('is Vue instance', () => {
    const wrapper = shallowMount(CultivationConsulting)
    expect(wrapper.vm).toBeTruthy()
  })
})
