import { shallowMount } from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import Contact from './../../pages/contact/index.vue'

describe('pages/contact/index.vue', () => {
  beforeEach(() => {
    Vue.use(Vuetify)
  })
  test('is Vue instance', () => {
    const wrapper = shallowMount(Contact)
    expect(wrapper.vm).toBeTruthy()
  })
})
