import { shallowMount } from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import MineralNutrition from './../../pages/mineral-nutrition-solutions/index.vue'

describe('pages/mineral-nutrition-solutions/index.vue', () => {
  beforeEach(() => {
    Vue.use(Vuetify)
  })
  test('is Vue instance', () => {
    const wrapper = shallowMount(MineralNutrition)
    expect(wrapper.vm).toBeTruthy()
  })
})
