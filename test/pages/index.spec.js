import { shallowMount } from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import Homepage from './../../pages/index.vue'

describe('pages/index.vue', () => {
  beforeEach(() => {
    Vue.use(Vuetify)
  })
  test('is Vue instance', () => {
    const wrapper = shallowMount(Homepage)
    expect(wrapper.vm).toBeTruthy()
  })
})
