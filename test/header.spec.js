import { mount, RouterLinkStub } from '@vue/test-utils'
import Header from './../layouts/header.vue'
import Vue from 'vue'
import Vuetify from 'vuetify'

describe('header.vue', () => {
  let vuetify, wrapper

  beforeEach(() => {
    vuetify = new Vuetify()

    Vue.use(Vuetify)
    wrapper = mount(Header, {
      stubs: {
        NuxtLink: RouterLinkStub,
      },
      vuetify,
    })
  })
  test('is Vue instance', () => {
    expect(wrapper).toBeTruthy()
  })

  test('display navigation after click, on small screens', async () => {
    // the lines below removes Vuetify warning
    const app = document.createElement('div')
    app.setAttribute('data-app', true)
    document.body.append(app)

    wrapper.setData({ lesserThan1024: false })

    const button = wrapper.find('.pfw-small-screen__falafel button')
    await button.trigger('click')

    expect(button.exists()).toBe(true)
    expect(button.attributes('aria-expanded')).toBe('true')
  })
})
