import { mount } from '@vue/test-utils'
import Title from '@/components/shared/title.vue'

describe('shared/title.vue', () => {
  test('is Vue instance', () => {
    const wrapper = mount(Title, {
      propsData: {
        title: 'Default title',
      },
    })
    expect(wrapper.vm).toBeTruthy()
  })
})
