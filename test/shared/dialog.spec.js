import { createLocalVue, mount } from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import Dialog from '@/components/shared/dialog.vue'

const localVue = createLocalVue()
//localVue.use(Vuetify)

describe('shared/dialog.vue', () => {
  // let vuetify = null
  let wrapper = null
  beforeEach(() => {
    //vuetify = new Vuetify()
    Vue.use(Vuetify)
    wrapper = mount(Dialog, {
      localVue,
      propsData: {
        text: 'default text',
        shouldDisplay: true,
      },
      stubs: [
        'v-dialog',
        'v-card',
        'v-card-title',
        'v-card-text',
        'v-divider',
        'v-card-actions',
        'v-spacer',
        'v-btn',
      ],
    })
  })
  test('is Vue instance', () => {
    expect(wrapper.vm).toBeTruthy()
  })
})
