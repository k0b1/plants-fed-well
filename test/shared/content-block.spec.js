import { mount } from '@vue/test-utils'
import ContentBlock from '@/components/shared/content-block.vue'

describe('shared/content-block.vue', () => {
  test('is Vue instance', () => {
    const wrapper = mount(ContentBlock, {
      propsData: {
        height: 'full-screen',
      },
    })
    expect(wrapper.vm).toBeTruthy()
  })
})
