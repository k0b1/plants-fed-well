import { mount } from '@vue/test-utils'
import Button from '@/components/shared/button.vue'

describe('shared/button.vue', () => {
  test('is a Vue instance ', () => {
    const wrapper = mount(Button, {
      propsData: {
        text: 'Test text',
      },
    })
    expect(wrapper.vm).toBeTruthy()
  })
})
