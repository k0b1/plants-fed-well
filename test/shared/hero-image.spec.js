import { mount } from '@vue/test-utils'
import HeroImage from '@/components/shared/hero-image.vue'

describe('shared/hero-image.vue', () => {
  test('is Vue instance', () => {
    const wrapper = mount(HeroImage, {
      propsData: {
        size: 'desktop',
        image: 'feed-plant-water.jpg',
      },
    })
    expect(wrapper.vm).toBeTruthy()
  })
})
