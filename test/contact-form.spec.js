import { mount } from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import ContactForm from '@/components/contact-form.vue'
import Dialog from '@/components/shared/dialog.vue'
import axios from 'axios'

// axios.$post.mockImplementation((url) => {
//     if (url === 'www.example.com') {
//         return Promise.resolve({ data: "success" });
//     } else {
//         //...
//       return "punk"
//     }
// });
// const localVue =
jest.mock('axios', () => ({
  post: jest.fn((_url, _body) => {
    //  url = _url;
    //  body = _body;
    return Promise.resolve()
  }),
}))
describe('contact-form.vue', () => {
  beforeEach(() => {
    Vue.use(Vuetify)
    //Vue.use(axios)
  })

  it('is Vue instance', () => {
    const wrapper = mount(ContactForm)
    expect(wrapper.vm).toBeTruthy()
  })

  it('successfull axios call', async () => {
    const wrapper = mount(ContactForm, {
      stubs: ['Dialog'],
    })
    wrapper.setData({
      form: {
        validate: () => true,
      },
    })

    await axios.post.mockImplementationOnce((response) =>
      Promise.resolve(response)
    )
    let params = new URLSearchParams()
    const button = wrapper.find('button')
    await button.trigger('click')

    expect(axios.post).toBeCalledWith(
      'https://plantsfedwell.ca/contact-me.php',
      expect.objectContaining(params)
    )

    expect(axios.post).toHaveBeenCalledTimes(1)
    expect(axios.post).toHaveBeenCalled()
    expect(wrapper.vm.isSuccessMessage).toBe(true)
  })

  it('unsuccessfull axios call', async () => {
    const wrapper = mount(ContactForm, {
      stubs: ['Dialog'],
    })
    wrapper.setData({
      form: {
        validate: () => true,
      },
    })

    await axios.post.mockImplementationOnce((response) =>
      Promise.reject(response)
    )
    let params = new URLSearchParams()
    const button = wrapper.find('button')
    await button.trigger('click')

    expect(axios.post).toBeCalledWith(
      'https://plantsfedwell.ca/contact-me.php',
      expect.objectContaining(params)
    )

    expect(axios.post).toHaveBeenCalledTimes(2)
    expect(axios.post).toHaveBeenCalled()
    expect(wrapper.vm.isErrorMessage).toBe(true)
  })
})
