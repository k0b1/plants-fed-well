import { mount, RouterLinkStub } from '@vue/test-utils'
import Logo from '@/components/Logo.vue'

describe('Logo', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(Logo, {
      stubs: {
        NuxtLink: RouterLinkStub,
      },
    })
    expect(wrapper.vm).toBeTruthy()
  })
  test('does nuxt link component exists?', () => {
    const wrapper = mount(Logo, {
      stubs: {
        NuxtLink: RouterLinkStub,
      },
    })
    const route = wrapper.findComponent(RouterLinkStub)
    expect(route.exists()).toBe(true)
  })
  test('should redirect properly?', () => {
    const wrapper = mount(Logo, {
      stubs: {
        NuxtLink: RouterLinkStub,
      },
    })
    const route = wrapper.findComponent(RouterLinkStub)
    expect(route.props('to')).toBe('/')
  })
  test('should increase logo size?', async () => {
    const wrapper = mount(Logo, {
      stubs: {
        NuxtLink: RouterLinkStub,
      },
    })
    const logoImage = wrapper.find('.pfw-logo__image');
    window.scrollY = 1000
    wrapper.vm.handleScroll();
    await wrapper.setData({screenWidth: '769'})
    await wrapper.setData({counter: '1'})
    expect(logoImage.classes()).toContain('should-decrease-logo-size')
  })
  test('should decrease logo size?', async () => {
    const wrapper = mount(Logo, {
      stubs: {
        NuxtLink: RouterLinkStub,
      },
    })
    const logoImage = wrapper.find('.pfw-logo__image');
    window.scrollY = 0
    wrapper.vm.handleScroll();
    await wrapper.setData({screenWidth: '769'})
    await wrapper.setData({counter: '1'})
    expect(logoImage.classes()).toContain('should-increase-logo-size')
  })
})
