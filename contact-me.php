<?php
if ($_POST['name'] && $_POST['email'] && $_POST['message'] && $_POST['phone'] && $_POST['city']) {
    $headers = "MIME-Version: 1.0" . "\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\n";

    $email_to_owner = 'office@plantsfedwell.ca';
    $subject_to_owner = 'New email for Plants fed well!';

    $email_to_sender = $_POST['email'];
    $subject_to_sender = "Plants Fed Well: Confirmation email message!";

    $message_to_owner = "
      <div style='margin-left:150px; padding:50px;width:600px;'>
        <h1 style='color:#000000;font-family: Arial, Helvetica, sans-serif;text-align:center;line-height:2.5em;'>Hello master! You have an new email!!!</h1>
        <hr>
       <table>
         <tr>
           <td style='text-align:center'>
             <div>
               <p style='color:#000000; font-family: Allura,cursive,Arial, Helvetica, sans-serif; font-size:20px'>
                 Name of sender: ". $_POST['name'] ."
               </p>
               <p style='color:#000000; font-family: Allura,cursive,Arial, Helvetica, sans-serif; font-size:20px'>
                 Email of sender: ". $_POST['email'] ."
               </p>
               <p style='color:#000000; font-family: Allura,cursive,Arial, Helvetica, sans-serif; font-size:20px'>
                 City of sender: ". $_POST['city'] ."
               </p>
            </div>
          </td>
       </tr>
       <tr>
         <td>
           <div style='float:left;'>
             <p style='color:#000000;font-family: Arial, Helvetica, sans-serif; font-size:20px'>
               Message: ". $_POST['message']."
             </p>
          </div>
         </td>
       </tr>
     </table>
   </div>";

    $message_to_sender = "
      <div style='margin-left:10px; padding:50px;width:600px;'>
       <table>
         <tr>
           <td style='text-align:left'>
             <div>
               <hr>
               <p style='color:#000000; font-family: Allura,cursive,Arial, Helvetica, sans-serif; font-size:20px'>
                 Thank you for contacting Plants Fed Well! We'll get in touch soon!
               </p>
               <hr>
            </div>
          </td>
       </tr>
     </table>
   </div>";

    mail($email_to_owner, $subject_to_owner, $message_to_owner, $headers);
    mail($email_to_sender, $subject_to_sender, $message_to_sender, $headers);
}
