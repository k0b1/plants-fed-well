export default {
  data() {
    return {
      screenWidth: 1025,
      size: '',
    }
  },
  methods: {},
  beforeMount() {
    this.screenWidth = screen.width
    if (this.screenWidth < 1024 && this.screenWidth > 600) {
      this.size = 'medium'
    } else if (this.screenWidth <= 600) {
      this.size = 'small'
    } else {
      this.size = 'desktop'
    }
  },
}
