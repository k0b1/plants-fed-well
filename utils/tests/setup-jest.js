import { config } from '@vue/test-utils';
import ClientOnly from "@/components/mocks/client-only-mock.vue"
// Mock Nuxt client-side component
config.stubs['client-only'] = ClientOnly;
